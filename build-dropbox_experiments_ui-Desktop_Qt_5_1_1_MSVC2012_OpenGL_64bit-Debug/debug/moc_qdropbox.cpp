/****************************************************************************
** Meta object code from reading C++ file 'qdropbox.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.1.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../dropbox_experiments_ui/dropbox/qdropbox.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qdropbox.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.1.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QDropbox_t {
    QByteArrayData data[27];
    char stringdata[354];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_QDropbox_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_QDropbox_t qt_meta_stringdata_QDropbox = {
    {
QT_MOC_LITERAL(0, 0, 8),
QT_MOC_LITERAL(1, 9, 12),
QT_MOC_LITERAL(2, 22, 0),
QT_MOC_LITERAL(3, 23, 15),
QT_MOC_LITERAL(4, 39, 9),
QT_MOC_LITERAL(5, 49, 12),
QT_MOC_LITERAL(6, 62, 12),
QT_MOC_LITERAL(7, 75, 17),
QT_MOC_LITERAL(8, 93, 9),
QT_MOC_LITERAL(9, 103, 20),
QT_MOC_LITERAL(10, 124, 5),
QT_MOC_LITERAL(11, 130, 6),
QT_MOC_LITERAL(12, 137, 19),
QT_MOC_LITERAL(13, 157, 12),
QT_MOC_LITERAL(14, 170, 19),
QT_MOC_LITERAL(15, 190, 11),
QT_MOC_LITERAL(16, 202, 16),
QT_MOC_LITERAL(17, 219, 12),
QT_MOC_LITERAL(18, 232, 18),
QT_MOC_LITERAL(19, 251, 10),
QT_MOC_LITERAL(20, 262, 17),
QT_MOC_LITERAL(21, 280, 12),
QT_MOC_LITERAL(22, 293, 15),
QT_MOC_LITERAL(23, 309, 2),
QT_MOC_LITERAL(24, 312, 14),
QT_MOC_LITERAL(25, 327, 4),
QT_MOC_LITERAL(26, 332, 20)
    },
    "QDropbox\0errorOccured\0\0QDropbox::Error\0"
    "errorcode\0tokenExpired\0fileNotFound\0"
    "operationFinished\0requestnr\0"
    "requestTokenFinished\0token\0secret\0"
    "accessTokenFinished\0tokenChanged\0"
    "accountInfoReceived\0accountJson\0"
    "metadataReceived\0metadataJson\0"
    "sharedLinkReceived\0sharedLink\0"
    "revisionsReceived\0revisionJson\0"
    "requestFinished\0nr\0QNetworkReply*\0"
    "rply\0networkReplyFinished\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QDropbox[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   79,    2, 0x05,
       5,    0,   82,    2, 0x05,
       6,    0,   83,    2, 0x05,
       7,    1,   84,    2, 0x05,
       9,    2,   87,    2, 0x05,
      12,    2,   92,    2, 0x05,
      13,    2,   97,    2, 0x05,
      14,    1,  102,    2, 0x05,
      16,    1,  105,    2, 0x05,
      18,    1,  108,    2, 0x05,
      20,    1,  111,    2, 0x05,

 // slots: name, argc, parameters, tag, flags
      22,    2,  114,    2, 0x08,
      26,    1,  119,    2, 0x08,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   10,   11,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   10,   11,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   10,   11,
    QMetaType::Void, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::QString,   17,
    QMetaType::Void, QMetaType::QString,   19,
    QMetaType::Void, QMetaType::QString,   21,

 // slots: parameters
    QMetaType::Void, QMetaType::Int, 0x80000000 | 24,   23,   25,
    QMetaType::Void, 0x80000000 | 24,   25,

       0        // eod
};

void QDropbox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QDropbox *_t = static_cast<QDropbox *>(_o);
        switch (_id) {
        case 0: _t->errorOccured((*reinterpret_cast< QDropbox::Error(*)>(_a[1]))); break;
        case 1: _t->tokenExpired(); break;
        case 2: _t->fileNotFound(); break;
        case 3: _t->operationFinished((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->requestTokenFinished((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 5: _t->accessTokenFinished((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 6: _t->tokenChanged((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 7: _t->accountInfoReceived((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 8: _t->metadataReceived((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->sharedLinkReceived((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->revisionsReceived((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->requestFinished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< QNetworkReply*(*)>(_a[2]))); break;
        case 12: _t->networkReplyFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 11:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 1:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 12:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QDropbox::*_t)(QDropbox::Error );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::errorOccured)) {
                *result = 0;
            }
        }
        {
            typedef void (QDropbox::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::tokenExpired)) {
                *result = 1;
            }
        }
        {
            typedef void (QDropbox::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::fileNotFound)) {
                *result = 2;
            }
        }
        {
            typedef void (QDropbox::*_t)(int );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::operationFinished)) {
                *result = 3;
            }
        }
        {
            typedef void (QDropbox::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::requestTokenFinished)) {
                *result = 4;
            }
        }
        {
            typedef void (QDropbox::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::accessTokenFinished)) {
                *result = 5;
            }
        }
        {
            typedef void (QDropbox::*_t)(QString , QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::tokenChanged)) {
                *result = 6;
            }
        }
        {
            typedef void (QDropbox::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::accountInfoReceived)) {
                *result = 7;
            }
        }
        {
            typedef void (QDropbox::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::metadataReceived)) {
                *result = 8;
            }
        }
        {
            typedef void (QDropbox::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::sharedLinkReceived)) {
                *result = 9;
            }
        }
        {
            typedef void (QDropbox::*_t)(QString );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QDropbox::revisionsReceived)) {
                *result = 10;
            }
        }
    }
}

const QMetaObject QDropbox::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QDropbox.data,
      qt_meta_data_QDropbox,  qt_static_metacall, 0, 0}
};


const QMetaObject *QDropbox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QDropbox::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_QDropbox.stringdata))
        return static_cast<void*>(const_cast< QDropbox*>(this));
    return QObject::qt_metacast(_clname);
}

int QDropbox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void QDropbox::errorOccured(QDropbox::Error _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QDropbox::tokenExpired()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void QDropbox::fileNotFound()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void QDropbox::operationFinished(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QDropbox::requestTokenFinished(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QDropbox::accessTokenFinished(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QDropbox::tokenChanged(QString _t1, QString _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QDropbox::accountInfoReceived(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QDropbox::metadataReceived(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QDropbox::sharedLinkReceived(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QDropbox::revisionsReceived(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}
QT_END_MOC_NAMESPACE
