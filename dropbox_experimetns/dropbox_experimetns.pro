#-------------------------------------------------
#
# Project created by QtCreator 2014-01-20T02:03:34
#
#-------------------------------------------------

QT       += core network xml

QT       += gui widgets

DEFINES += QTDROPBOX_LIBRARY QTDROPBOX_DEBUG

TARGET = dropbox_experimetns
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    dropbox/qdropboxjson.cpp \
    dropbox/qdropboxfileinfo.cpp \
    dropbox/qdropboxfile.cpp \
    dropbox/qdropboxaccount.cpp \
    dropbox/qdropbox.cpp

HEADERS += \
    dropbox/qtdropbox_global.h \
    dropbox/qtdropbox.h \
    dropbox/qdropboxjson.h \
    dropbox/qdropboxfileinfo.h \
    dropbox/qdropboxfile.h \
    dropbox/qdropboxaccount.h \
    dropbox/qdropbox.h
