#ifndef DROPBOX_CONNECTOR_H
#define DROPBOX_CONNECTOR_H

#include "dropbox/qtdropbox.h"
#include <QPushButton>
#include <QDesktopServices>

class dropbox_connector : public QObject
{
    Q_OBJECT

public:
    dropbox_connector(QPushButton* btn);

private:
    QDropbox* m_api;
    QPushButton* m_btn;

private slots:
    void btn_clicked();

};

#endif // DROPBOX_CONNECTOR_H
