#-------------------------------------------------
#
# Project created by QtCreator 2014-01-20T18:52:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += network xml

DEFINES += QTDROPBOX_LIBRARY QTDROPBOX_DEBUG

TARGET = dropbox_experiments_ui
TEMPLATE = app


SOURCES += main.cpp\
        main_window.cpp \
    dropbox/qdropboxjson.cpp \
    dropbox/qdropboxfileinfo.cpp \
    dropbox/qdropboxfile.cpp \
    dropbox/qdropboxaccount.cpp \
    dropbox/qdropbox.cpp \
    dropbox_connector.cpp

HEADERS  += main_window.h \
    dropbox/qtdropbox_global.h \
    dropbox/qtdropbox.h \
    dropbox/qdropboxjson.h \
    dropbox/qdropboxfileinfo.h \
    dropbox/qdropboxfile.h \
    dropbox/qdropboxaccount.h \
    dropbox/qdropbox.h \
    dropbox_connector.h
